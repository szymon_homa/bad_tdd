/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.bad.tdd;

/**
 *
 * @author shoma
 */
public class IncomingGoodsDistpatcher {

	private final DispatcherEventsListener eventsListener;
	private final ContainerWithGoodsMatcher withGoodsMatcher;

	public IncomingGoodsDistpatcher(DispatcherEventsListener eventsListener, ContainerWithGoodsMatcher withGoodsMatcher) {
		this.eventsListener = eventsListener;
		this.withGoodsMatcher = withGoodsMatcher;
	}

	public void dispatch(Goods goods) {
		Iterable<Container> containers = withGoodsMatcher.getAvailableContainersForGoods(goods);
		for (Container container : containers) {
			eventsListener.sendToAWarehouse(goods, container);
		}
		eventsListener.rejectDueToLackOfContainer(goods);
	}
}
