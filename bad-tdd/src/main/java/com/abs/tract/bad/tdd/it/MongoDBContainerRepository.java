/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.bad.tdd.it;

import com.abs.tract.bad.tdd.Container;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author shoma
 */
@Repository
public class MongoDBContainerRepository implements ContainerRepository {

	private final SpringContainerMongoDBUtilRepository springUtilRepository;

	@Autowired
	public MongoDBContainerRepository(SpringContainerMongoDBUtilRepository springUtilRepository) {
		this.springUtilRepository = springUtilRepository;
	}

	public Container getByCode(String code) throws IllegalStateException {
		return springUtilRepository.findOne(code);
	}

	public List<Container> getAll() {
		return springUtilRepository.findAll();
	}

	public void add(Container container) {
		springUtilRepository.save(container);
	}

	public void update(Container container) {
		springUtilRepository.save(container);
	}
}
