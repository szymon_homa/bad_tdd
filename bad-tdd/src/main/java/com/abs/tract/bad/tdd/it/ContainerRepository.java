/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.bad.tdd.it;

import com.abs.tract.bad.tdd.Container;
import java.util.List;

/**
 *
 * @author shoma
 */
public interface ContainerRepository {

	Container getByCode(String code);

	List<Container> getAll();

	void add(Container container);

	void update(Container container);
}
