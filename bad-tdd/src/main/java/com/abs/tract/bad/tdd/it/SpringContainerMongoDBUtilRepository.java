/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.bad.tdd.it;

import com.abs.tract.bad.tdd.Container;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 *
 * @author shoma
 */
public interface SpringContainerMongoDBUtilRepository extends MongoRepository<Container, String> {
}
