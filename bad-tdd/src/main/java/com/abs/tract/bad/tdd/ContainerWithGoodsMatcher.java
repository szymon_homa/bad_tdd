/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.bad.tdd;

/**
 *
 * @author shoma
 */
public interface ContainerWithGoodsMatcher {

	public Iterable<Container> getAvailableContainersForGoods(Goods goods);
}
