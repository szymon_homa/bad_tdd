/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.bad.tdd;

import java.util.Objects;

/**
 *
 * @author shoma
 */
public class Goods {

	private final Type type;
	private final String name;
	private final int quantity;
	private final int weight;

	public Goods(Type type, String name, int quantity, int weight) {
		this.type = type;
		this.name = name;
		this.quantity = quantity;
		this.weight = weight;
	}

	public int getWeight() {
		return weight;
	}

	public String getName() {
		return name;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && this.getClass().equals(obj.getClass())) {
			Goods goods = (Goods) obj;
			return Objects.equals(type, goods.type)
				&& Objects.equals(name, goods.name)
				&& Objects.equals(quantity, goods.quantity)
				&& Objects.equals(weight, goods.weight);
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 61 * hash + (this.type != null ? this.type.hashCode() : 0);
		hash = 61 * hash + (this.name != null ? this.name.hashCode() : 0);
		hash = 61 * hash + this.quantity;
		hash = 61 * hash + this.weight;
		return hash;
	}

	public static enum Type {

		Food,
		ElectricEquipment,
		Arms
	}
}
