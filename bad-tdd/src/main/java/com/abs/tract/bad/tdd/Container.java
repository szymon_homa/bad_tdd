/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.bad.tdd;

import java.util.Objects;
import org.springframework.data.annotation.Id;

/**
 *
 * @author shoma
 */
public class Container {

	@Id
	private final String code;
	private int availableLoad;

	public Container(String code, int availableLoad) {
		this.code = code;
		this.availableLoad = availableLoad;
	}

	public String getCode() {
		return code;
	}

	public int getAvailableLoadWeight() {
		return availableLoad;
	}

	public void reduceAvailableWeightBy(int justLoadedItemWeight) {
		availableLoad = availableLoad - justLoadedItemWeight;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && getClass().equals(obj.getClass())) {
			Container container = (Container) obj;
			return Objects.equals(code, container.code);
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 73 * hash + (this.code != null ? this.code.hashCode() : 0);
		return hash;
	}
}
