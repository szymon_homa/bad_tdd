/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.bad.tdd;

/**
 *
 * @author shoma
 */
public interface DispatcherEventsListener {

	public void sendToAWarehouse(Goods goods, Container container);

	public void rejectDueToLackOfContainer(Goods goods);
}
