/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.bad.tdd.it;

import com.mongodb.MongoClient;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 *
 * @author shoma
 */
@Configuration
@ComponentScan(basePackages = "com.abs.tract.bad.tdd.it")
@EnableMongoRepositories(basePackages = "com.abs.tract.bad.tdd.it")
public class TestPersistenceContext {

	private final String databaseName = "badtdd";
	private final String databaseUrl = "localhost";

	@Bean
	public MongoDbFactory mongoDbFactory() {
		try {
			return new SimpleMongoDbFactory(new MongoClient(databaseUrl), databaseName);
		} catch (UnknownHostException ex) {
			throw new IllegalStateException(ex);
		}
	}

	@Bean
	public MongoTemplate mongoTemplate() {
		return new MongoTemplate(mongoDbFactory());
	}
}
