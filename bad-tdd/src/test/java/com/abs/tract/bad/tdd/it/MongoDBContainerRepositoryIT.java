/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.bad.tdd.it;

import com.abs.tract.bad.tdd.Container;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import static org.junit.Assert.*;
import static com.abs.tract.bad.tdd.it.ContainerFixture.*;

/**
 *
 * @author shoma
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners(value = {DependencyInjectionTestExecutionListener.class})
@ContextConfiguration(classes = {TestPersistenceContext.class})
public class MongoDBContainerRepositoryIT {

	private final String containerCode = "containerCode";
	@Autowired
	private MongoTemplate mongoTemplate;
	@Autowired
	private ContainerRepository containerRepository;

	@Before
	public void setUp() {
		mongoTemplate.getDb().dropDatabase();
	}

	@Test
	public void testAddOneContainer() {
		Container newContainer = createContainer(containerCode);

		containerRepository.add(newContainer);

		Container retrieved = containerRepository.getByCode(containerCode);
		assertEquals(retrieved, newContainer);
	}

	@Test
	public void testAddManyContainers() {
		String firstContainerCode = "firstContainerCode";
		String secondContainerCode = "secondContainerCode";
		Container firstContainer = createContainer(firstContainerCode);
		Container secondContainer = createContainer(secondContainerCode);

		containerRepository.add(firstContainer);
		containerRepository.add(secondContainer);

		List<Container> expectedContainers = containerRepository.getAll();
		assertEquals(expectedContainers, Arrays.asList(firstContainer, secondContainer));
	}

	@Test
	public void testUpdatePreviouslyAddedContainer() {
		int initialLoad = 23;
		Container toUpdate = createContainerWithAvailableLoad(initialLoad);
		containerRepository.add(toUpdate);

		int itemWeight = 8;
		toUpdate.reduceAvailableWeightBy(itemWeight);
		containerRepository.update(toUpdate);

		int expectedAvailableLoadWeight = initialLoad - itemWeight;
		Container updated = containerRepository.getByCode(toUpdate.getCode());
		assertEquals(expectedAvailableLoadWeight, updated.getAvailableLoadWeight());
	}
}