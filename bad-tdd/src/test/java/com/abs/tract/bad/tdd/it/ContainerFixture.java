/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.bad.tdd.it;

import com.abs.tract.bad.tdd.Container;

/**
 *
 * @author shoma
 */
public class ContainerFixture {

	public static Container createContainer(String code) {
		return new Container(code, 10);
	}

	public static Container createContainerWithAvailableLoad(int availableLoad) {
		return new Container("containerCode", availableLoad);
	}
}
