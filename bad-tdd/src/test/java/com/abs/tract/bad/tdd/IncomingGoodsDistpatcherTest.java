/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.bad.tdd;

import java.util.Arrays;
import java.util.Collections;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author shoma
 */
public class IncomingGoodsDistpatcherTest {

	private final DispatcherEventsListener eventsListener = mock(DispatcherEventsListener.class);
	private final ContainerWithGoodsMatcher goodsWithContainerMatcher = mock(ContainerWithGoodsMatcher.class);
	private final IncomingGoodsDistpatcher incommingGoodsDistpatcher = new IncomingGoodsDistpatcher(eventsListener, goodsWithContainerMatcher);

	public IncomingGoodsDistpatcherTest() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testDispatchingGoodsWhenGoodsFitsIntoASingleContainer() {
		Goods food = createFood();
		Container expectedContainer = createContainerForGoods(food);
		when(goodsWithContainerMatcher.getAvailableContainersForGoods(food)).thenReturn(Arrays.asList(expectedContainer));

		incommingGoodsDistpatcher.dispatch(food);

		verify(eventsListener).sendToAWarehouse(food, expectedContainer);
	}

	@Test
	public void testDispatchingGoodsWhenNoContainersAreAvailable() {
		Goods food = createFood();
		when(goodsWithContainerMatcher.getAvailableContainersForGoods(food)).thenReturn(Collections.EMPTY_LIST);

		incommingGoodsDistpatcher.dispatch(food);

		verify(eventsListener).rejectDueToLackOfContainer(food);
	}

	@Test
	public void testDispatchingGoodsWhenManyContainersNeeded() {
		Goods food = createFood();
		Container firstContainer = createContainerForWeight("firstContainer", food.getWeight() / 2);
		Container secondContainer = createContainerForWeight("secondContainer", food.getWeight() / 2);
		when(goodsWithContainerMatcher.getAvailableContainersForGoods(food)).thenReturn(Arrays.asList(firstContainer, secondContainer));

		incommingGoodsDistpatcher.dispatch(food);

		verify(eventsListener, times(2)).sendToAWarehouse(any(Goods.class), any(Container.class));
	}

	@Test
	public void testDispatchingGoodsWhenOnlyOneUnsufficientContainerIsAvailable() {
		Goods food = createFood();
		Container unsufficientContainer = createContainerForWeight("firstContainer", food.getWeight() / 2);
		when(goodsWithContainerMatcher.getAvailableContainersForGoods(food)).thenReturn(Arrays.asList(unsufficientContainer));

		incommingGoodsDistpatcher.dispatch(food);

		verify(eventsListener).sendToAWarehouse(any(Goods.class), any(Container.class));
		verify(eventsListener).rejectDueToLackOfContainer(any(Goods.class));
	}

	private Goods createFood() {
		return new Goods(Goods.Type.Food, "food", 10, 10);
	}

	private Container createContainerForGoods(Goods food) {
		Container expectedContainer = new Container(food.getName() + " container", food.getWeight());
		return expectedContainer;
	}

	private Container createContainerForWeight(String containerCode, int availableWeightRequired) {
		Container expectedContainer = new Container(containerCode, availableWeightRequired);
		return expectedContainer;
	}
}