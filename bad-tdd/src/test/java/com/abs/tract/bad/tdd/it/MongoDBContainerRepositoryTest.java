/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.bad.tdd.it;

import com.abs.tract.bad.tdd.Container;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static com.abs.tract.bad.tdd.it.ContainerFixture.*;

/**
 *
 * @author shoma
 */
public class MongoDBContainerRepositoryTest {

	private final SpringContainerMongoDBUtilRepository dbUtilRepository = mock(SpringContainerMongoDBUtilRepository.class);
	private final MongoDBContainerRepository containerRepository = new MongoDBContainerRepository(dbUtilRepository);
	private final String containerCode = "containerCode";

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testAddContainer() {
		Container container = createContainer(containerCode);

		containerRepository.add(container);

		verify(dbUtilRepository).save(container);
	}

	@Test
	public void testGetByCode() {
		Container container = createContainer(containerCode);
		when(dbUtilRepository.findOne(containerCode)).thenReturn(container);

		Container retrieved = containerRepository.getByCode(containerCode);

		assertSame(container, retrieved);
	}

	@Test
	public void testUpdate() {
		Container container = createContainer(containerCode);

		containerRepository.update(container);

		verify(dbUtilRepository).save(container);
	}
}