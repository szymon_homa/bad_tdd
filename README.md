## What is this repository for? ##

This repo holds actually two examples

* first - described by a first commit - is an example of almost good looking unit tests, which doesn't cover any of the required logic (a little bit nastier example of [assertion free tests](http://martinfowler.com/bliki/AssertionFreeTesting.html)). 
* second - described by a second commit - is an example of a unit test that was written just to follow the "rules", where the real benefit comes with an integration test.

## How to execute the application ##

There is nothing to execute here - just plain maven compilation. If someone is interested in seeing that the seond example actually works, the up and running mongodb installation is required.

## Who is to blame? ##
if you have questions, threats or love letters to send, use this email: 
s2lomon@gmail.com.